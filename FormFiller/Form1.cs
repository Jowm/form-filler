﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using iTextSharp.text.pdf;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Collections;



namespace FormFiller
{
    public partial class Form1 : Form
    {
        public static ArrayList arrInflow = new ArrayList();
        Form2 asd = new Form2();
        Form2 form2 = new Form2();
        DataTable dtInflow = new DataTable();

        public static ArrayList arrList1 = new ArrayList();
        public static ArrayList arrList2 = new ArrayList();
        public static string filename = "";
        public static int id = 0;
        public static string propID = "";
        public static string pdfTemplate = "";
        public static string nameofpdf = "";

        //public static int count = 0;

        bool ctTag = false;
        bool ctDone = false;

        public Form1()
        {
            InitializeComponent();
            lblUser.Text = System.DirectoryServices.AccountManagement.UserPrincipal.Current.Surname + ", " + System.DirectoryServices.AccountManagement.UserPrincipal.Current.GivenName;
            string[] files = System.IO.Directory.GetFiles(@"\\ascorp.com\data\bangalore\CommonShare\Strategic$\Internal\Operations\VPR Manila\PPI-MIS Manila\Dump - Raw File\PDF Files\");
            //string[] files = System.IO.Directory.GetFiles(@"C:\Users\orculloj\Desktop\PDF\");

            foreach (string file in files)
                cboTemplate.Items.Add(Path.GetFileName(file));
            cboPath.Items.AddRange(files);

        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void splitContainer2_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btnLoad_Click(sender, e);
            timer1.Enabled = true;

            if (rbGood.Checked)
            {
                cbErrors.Hide();
            }
            else
            {
                cbErrors.Show();
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            

            webRight.Stop();
            webRight.Hide();
            webRight.Navigate("about:blank");
            do
            {
                Application.DoEvents();
                System.Threading.Thread.Sleep(100);

            } while (webRight.ReadyState != WebBrowserReadyState.Complete);
            System.Threading.Thread.Sleep(100);
            webRight.Navigate("about:blank");
            webRight.Show();


            clsConnection cls = new clsConnection();
            cls.setConn("DAV");
            DataTable inflowDT = new DataTable();
            DataTable municipalityDT = new DataTable();

            //string query = "select top 1 * from view_VPR_Inflow where lockto='" + Environment.UserName + "' order by case when [Prio Tag] is null then 1 else 0 end, [Prio Tag], [Total Aging] desc";
            string query = "select * from tbl_VPR_CreatedFile where [Property ID] = '71418321591'";
            //string query = "select * from tbl_VPR_CreatedFile where id = '30557'";
            //string query = "select top 1 * from view_VPR_Inflow where id = '25278'"; //test
            
            inflowDT = cls.GetData(query);

            id = int.Parse(inflowDT.Rows[0]["id"].ToString());
            propID = inflowDT.Rows[0]["Property ID"].ToString();
            lblId.Text = inflowDT.Rows[0]["id"].ToString();
            string a1 = inflowDT.Rows[0]["Municipality"].ToString();
            string a2 = inflowDT.Rows[0]["VPR Service Type"].ToString();
            string a3 = inflowDT.Rows[0]["Product"].ToString();
            string a4 = inflowDT.Rows[0]["Property ID"].ToString();
            string a5 = inflowDT.Rows[0]["State"].ToString();
            string a6 = inflowDT.Rows[0]["Property Address"].ToString();
            string a7 = inflowDT.Rows[0]["Municipality Code"].ToString();
            string a8 = inflowDT.Rows[0]["Product"].ToString();
            string a9 = inflowDT.Rows[0]["Occupancy Status"].ToString();
            string fch = inflowDT.Rows[0]["FC/FCH Codes"].ToString();

            AddToList(a1, a2, a3, a4, a5, a9, fch);

            lblProp.Text = "PropID: " + inflowDT.Rows[0]["Property ID"].ToString();
            lblProp.BackColor = this.BackColor;
            lblState.Text = "State: " + inflowDT.Rows[0]["State"].ToString();
            lblAdd.Text = "Address: " + inflowDT.Rows[0]["Property Address"].ToString();
            lblMunicipality.Text = "Municipality: " + inflowDT.Rows[0]["Municipality Name"].ToString();
            lblProduct.Text = "Product: " + inflowDT.Rows[0]["Product"].ToString();

            string[] arrCT = { "CT - Deep River", "CT - Derby", "New Haven", "CT - Waterbury" };

            if (a1.IndexOf("CT - ") > -1 &&
                (arrCT.Contains(a1) || a1.IndexOf("New Haven") > -1) &&
                !ctDone)
            {
                ctTag = true;
            }
            else
            {
                ctTag = false;
            }

            query = "select top 1 [file_name] from [tbl_VPR_Municipality] where [municipality_code]='" +
                inflowDT.Rows[0]["Municipality Code"].ToString() + "' and client='" + inflowDT.Rows[0]["Product"].ToString() +
                "' and [reg_type]='" + inflowDT.Rows[0]["VPR Service Type"].ToString() + "' and [file_name] is not null and isActive = 1";
            //query = "select top 1 [file_name] from [tbl_VPR_Municipality] where [municipality_code]='530154548' and client='REO' and [reg_type]='Property Registration' and [file_name] is not null";

            municipalityDT = cls.GetData(query);


            
            if (municipalityDT.Rows.Count == 0)
            {
                MessageBox.Show("No available template for  " + a7);

                query = "update tbl_VPR_CreatedFile set status='Processed',processedBy='" + Environment.UserName + "', Reason = 'No template',InitialProcessor='" + Environment.UserName + "',processedDate='" + DateTime.Now + "',HandleTime='" + lblHT.Text + "'  where id='" + lblId.Text + "'";
                cls.ExecuteQuery(query);

                cls.ExecuteQuery("update tbl_VPR_CreatedFile set lockto='' where lockto='" + Environment.UserName + "'");

                this.Close();

                //Process.Start("ProChampsTest.exe");
                //Process.Start("FormFiller.exe");
                Process.Start("ProChamps.exe");

            }

            string file1 = @"\\ascorp.com\data\bangalore\CommonShare\Strategic$\Internal\Operations\VPR Manila\PPI-MIS Manila\Dump - Raw File\PDF Files\";
            //string file1 = @"C:\Users\orculloj\Desktop\PDF\";

            string pdfTemplate = "";

            if (ctTag)
            {
                pdfTemplate = file1 + ((a8 == "REO") ? "R" : "P" + "I_000000053_CT - Statewide.pdf");
            }
            else
            {
                pdfTemplate = file1 + municipalityDT.Rows[0][0].ToString();
            }


            string[] strfile = pdfTemplate.Split('\\');


            nameofpdf = strfile[13].ToString();


            lblPath.Text = pdfTemplate;

            string newFile = System.IO.Directory.GetCurrentDirectory() + "_New.pdf";

            populateDataGrid(pdfTemplate, newFile, arrInflow);

            button2.Enabled = true;
            button1.Enabled = true;
            btnLoad.Enabled = false;

        }

        public void populateDataGrid(string pdfTemplate, string newFile, ArrayList arrInflow)
        {
            try
            {
                PdfReader pdfReader = new PdfReader(pdfTemplate);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));
                AcroFields pdfFormFields = pdfStamper.AcroFields;
                dgrid.Rows.Clear();
                DataTable dt = new DataTable();
                clsConnection cls = new clsConnection();
                cls.setConn("DAV");
                

                foreach (var f in pdfFormFields.Fields)
                {
                    if (f.Key.Contains("__") == false)
                    {
                        string fkey = f.Key;

                        if (fkey.IndexOf("mc_") > -1)
                        {
                            fkey = fkey.Replace("mc_", "");

                            dgrid1.Rows.Add(fkey, "");
                            arrList2.Add(fkey);
                        }

                        dgrid.Rows.Add(fkey, "");
                        arrList1.Add(fkey);
                    }
                    else
                    {
                        string rplc = f.Key.Remove(f.Key.IndexOf("__"), f.Key.Length - f.Key.IndexOf("__"));
                        if (!arrList1.Contains(rplc))
                        {
                            string fkey = rplc;

                            if (fkey.IndexOf("mc_") > -1)
                            {
                                fkey = fkey.Replace("mc_", "");
                            }

                            dgrid.Rows.Add(fkey);
                            arrList1.Add(fkey);
                        }

                        dgrid1.Rows.Add(f.Key, "");
                        arrList2.Add(f.Key);
                    }
                }

                pdfStamper.FormFlattening = false;
                pdfStamper.Close();
                pdfReader.Close();
                getFromDB();

                for (int x = 0; x < dgrid1.Rows.Count; x++)
                {
                    String a = dgrid1.Rows[x].Cells[0].Value.ToString();

                    if (a.Substring(0, 2) == "mc")
                    {
                        if (a.IndexOf("__") > -1)
                        {
                            a = a.Substring(0, a.IndexOf("__"));
                            a = a.Substring(3, (a.Length) - 3);
                        }
                        //else
                        //{
                        //    a = a.Substring(0, 2);
                        //}
                    }
                    else
                    {
                        if (a.IndexOf("__") > -1)
                        {
                            a = a.Substring(0, a.IndexOf("__"));
                        }
                    }


                    if (a != "")
                    {
                        for (int y = 0; y < dgrid.Rows.Count; y++)
                        {
                            if (dgrid.Rows[y].Cells[0].Value.ToString() == a)
                            {
                                dgrid1.Rows[x].Cells[1].Value = dgrid.Rows[y].Cells[1].Value;
                            }
                        }
                    }
                }

                pdfTemplate = newFile;

                newFile = System.IO.Directory.GetCurrentDirectory() + "_New1.pdf";

                pdfReader = new PdfReader(pdfTemplate);

                try
                {
                    pdfStamper = new PdfStamper(pdfReader, new FileStream(
                     newFile, FileMode.Create));
                }
                catch (Exception ex)
                {
                    newFile = System.IO.Directory.GetCurrentDirectory() + "_New3.pdf";

                    pdfStamper = new PdfStamper(pdfReader, new FileStream(
                     newFile, FileMode.Create));
                }
                pdfFormFields = pdfStamper.AcroFields;

                if (dgrid.Rows.Count > 0)
                {
                    for (int x = 0; x < dgrid.Rows.Count; x++)
                    {
                        if (dgrid.Rows[x].Cells[0].Value.ToString().IndexOf("_txt") > -1)
                        {
                            string query = "select * from tbl_VPR_Mapping_Conditions where field_name = '" + dgrid.Rows[x].Cells[0].Value.ToString().Replace("_txt", "") + "'";
                            DataTable dt1 = cls.GetData(query);

                            string optr, col_name, col_val, cond, optr2, col_name2, col_val2, cond2, optr3, col_name3, col_val3, cond3 = "";
                            int a = 0, b = 0, c = 0, ctf = 0;

                            if (dt1.Rows.Count > 0)
                            {
                                if (dt1.Rows[0]["column_name"].ToString() != "")
                                {
                                    if (dt1.Rows[0]["column_name"].ToString() == "Property Type")
                                    {
                                        string qryNorm = "select * from tbl_VPR_PropertyType_Normalize where property_type = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        col_name = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                    else if (dt1.Rows[0]["column_name"].ToString() == "Occupancy Status")
                                    {
                                        string qryNorm = "select * from tbl_VPR_OccupancyStatus_Normalize where occupancy_status = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        col_name = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                    else
                                    {
                                        col_name = dtInflow.Rows[0][dt1.Rows[0]["column_name"].ToString()].ToString();
                                    }
                                    optr = dt1.Rows[0]["logical_operator"].ToString();
                                    col_val = dt1.Rows[0]["column_val"].ToString();

                                    if (!CheckCondition(optr, col_name, col_val))
                                    {
                                        a++;
                                    }
                                }

                                if (dt1.Rows[0]["column_name2"].ToString() != "")
                                {
                                    if (dt1.Rows[0]["column_name2"].ToString() == "Property Type")
                                    {
                                        string qryNorm = "select * from tbl_VPR_PropertyType_Normalize where property_type = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name2"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        col_name2 = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                    else if (dt1.Rows[0]["column_name2"].ToString() == "Occupancy Status")
                                    {
                                        string qryNorm = "select * from tbl_VPR_OccupancyStatus_Normalize where occupancy_status = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name2"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        col_name2 = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                    else
                                    {
                                        col_name2 = dtInflow.Rows[0][dt1.Rows[0]["column_name2"].ToString()].ToString();
                                    }
                                    optr2 = dt1.Rows[0]["logical_operator2"].ToString();
                                    col_val2 = dt1.Rows[0]["column_val2"].ToString();

                                    if (!CheckCondition(optr2, col_name2, col_val2))
                                    {
                                        b++;
                                    }
                                }

                                if (dt1.Rows[0]["column_name3"].ToString() != "")
                                {
                                    if (dt1.Rows[0]["column_name3"].ToString() == "Property Type")
                                    {
                                        string qryNorm = "select * from tbl_VPR_PropertyType_Normalize where property_type = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name3"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        col_name3 = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                    else if (dt1.Rows[0]["column_name3"].ToString() == "Occupancy Status")
                                    {
                                        string qryNorm = "select * from tbl_VPR_OccupancyStatus_Normalize where occupancy_status = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name3"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        col_name3 = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                    else
                                    {
                                        col_name3 = dtInflow.Rows[0][dt1.Rows[0]["column_name3"].ToString()].ToString();
                                    }
                                    optr3 = dt1.Rows[0]["logical_operator3"].ToString();
                                    col_val3 = dt1.Rows[0]["column_val3"].ToString();

                                    if (!CheckCondition(optr3, col_name3, col_val3))
                                    {
                                        c++;
                                    }
                                }

                                cond = dt1.Rows[0]["and_or_val"].ToString();
                                cond2 = dt1.Rows[0]["and_or_val2"].ToString();

                                int field1 = (a > 0) ? 0 : 1;
                                int field2 = (b > 0) ? 0 : 1;
                                int field3 = (c > 0) ? 0 : 1;

                                DataTable dtLogic = new DataTable();
                                if (cond != "")
                                {
                                    string qryLogic = "select * from tbl_VPR_Logic where ";
                                    qryLogic += "a = '" + field1 + "' and optr1 = '" + cond.Trim().ToLower() + "' and b = '" + field2 + "' and optr2 = '" + cond2.Trim().ToLower() + "' and c = '" + field3 + "'";
                                    dtLogic = cls.GetData(qryLogic);
                                }

                                if (dtLogic.Rows.Count > 0)
                                {
                                    if (dtLogic.Rows[0]["action"].ToString() == "1")
                                    {
                                        pdfFormFields.SetField("mc_" + dgrid.Rows[x].Cells[0].Value.ToString(), dgrid.Rows[x].Cells[1].Value == null ? "" : dgrid.Rows[x].Cells[1].Value.ToString());

                                        pdfFormFields.SetField("mc_" + dgrid.Rows[x].Cells[0].Value.ToString() + "__2", dgrid.Rows[x].Cells[1].Value == null ? "" : dgrid.Rows[x].Cells[1].Value.ToString());
                                    }
                                }
                                else
                                {
                                    if (field1 != 0)
                                    {
                                        pdfFormFields.SetField("mc_" + dgrid.Rows[x].Cells[0].Value.ToString(), dgrid.Rows[x].Cells[1].Value == null ? "" : dgrid.Rows[x].Cells[1].Value.ToString());

                                        pdfFormFields.SetField("mc_" + dgrid.Rows[x].Cells[0].Value.ToString() + "__2", dgrid.Rows[x].Cells[1].Value == null ? "" : dgrid.Rows[x].Cells[1].Value.ToString());
                                    }
                                }
                            }
                        }
                        else if (dgrid.Rows[x].Cells[0].Value.ToString().IndexOf("_yn") > -1)
                        {
                            string query = "select * from tbl_VPR_Mapping_Conditions where field_name = '" + dgrid.Rows[x].Cells[0].Value.ToString().Replace("_yn", "") + "'";
                            DataTable dt1 = cls.GetData(query);

                            string optr, col_name, col_val, cond, optr2, col_name2, col_val2, cond2, optr3, col_name3, col_val3, cond3 = "";
                            int a = 0, b = 0, c = 0, ctf = 0;

                            if (dt1.Rows.Count > 0)
                            {
                                if (dt1.Rows[0]["column_name"].ToString() != "")
                                {
                                    if (dt1.Rows[0]["column_name"].ToString() == "Property Type")
                                    {
                                        string qryNorm = "select * from tbl_VPR_PropertyType_Normalize where property_type = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        col_name = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                    else if (dt1.Rows[0]["column_name"].ToString() == "Occupancy Status")
                                    {
                                        string qryNorm = "select * from tbl_VPR_OccupancyStatus_Normalize where occupancy_status = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        col_name = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                    else
                                    {
                                        col_name = dtInflow.Rows[0][dt1.Rows[0]["column_name"].ToString()].ToString();
                                    }
                                    optr = dt1.Rows[0]["logical_operator"].ToString();
                                    col_val = dt1.Rows[0]["column_val"].ToString();

                                    if (!CheckCondition(optr, col_name, col_val))
                                    {
                                        a++;
                                    }
                                }

                                if (dt1.Rows[0]["column_name2"].ToString() != "")
                                {
                                    if (dt1.Rows[0]["column_name2"].ToString() == "Property Type")
                                    {
                                        string qryNorm = "select * from tbl_VPR_PropertyType_Normalize where property_type = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name2"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        col_name2 = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                    else if (dt1.Rows[0]["column_name2"].ToString() == "Occupancy Status")
                                    {
                                        string qryNorm = "select * from tbl_VPR_OccupancyStatus_Normalize where occupancy_status = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name2"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        col_name2 = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                    else
                                    {
                                        col_name2 = dtInflow.Rows[0][dt1.Rows[0]["column_name2"].ToString()].ToString();
                                    }
                                    optr2 = dt1.Rows[0]["logical_operator2"].ToString();
                                    col_val2 = dt1.Rows[0]["column_val2"].ToString();

                                    if (!CheckCondition(optr2, col_name2, col_val2))
                                    {
                                        b++;
                                    }
                                }

                                if (dt1.Rows[0]["column_name3"].ToString() != "")
                                {
                                    if (dt1.Rows[0]["column_name3"].ToString() == "Property Type")
                                    {
                                        string qryNorm = "select * from tbl_VPR_PropertyType_Normalize where property_type = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name3"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        col_name3 = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                    else if (dt1.Rows[0]["column_name3"].ToString() == "Occupancy Status")
                                    {
                                        string qryNorm = "select * from tbl_VPR_OccupancyStatus_Normalize where occupancy_status = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name3"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        col_name3 = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                    else
                                    {
                                        col_name3 = dtInflow.Rows[0][dt1.Rows[0]["column_name3"].ToString()].ToString();
                                    }
                                    optr3 = dt1.Rows[0]["logical_operator3"].ToString();
                                    col_val3 = dt1.Rows[0]["column_val3"].ToString();

                                    if (!CheckCondition(optr3, col_name3, col_val3))
                                    {
                                        c++;
                                    }
                                }

                                cond = dt1.Rows[0]["and_or_val"].ToString();
                                cond2 = dt1.Rows[0]["and_or_val2"].ToString();

                                int field1 = (a > 0) ? 0 : 1;
                                int field2 = (b > 0) ? 0 : 1;
                                int field3 = (c > 0) ? 0 : 1;

                                DataTable dtLogic = new DataTable();
                                if (cond != "")
                                {
                                    string qryLogic = "select * from tbl_VPR_Logic where ";
                                    qryLogic += "a = '" + field1 + "' and optr1 = '" + cond.Trim().ToLower() + "' and b = '" + field2 + "' and optr2 = '" + cond2.Trim().ToLower() + "' and c = '" + field3 + "'";
                                    dtLogic = cls.GetData(qryLogic);
                                }

                                if (dtLogic.Rows.Count > 0)
                                {
                                    if (dtLogic.Rows[0]["action"].ToString() == "1")
                                    {
                                        pdfFormFields.SetField("mc_" + dgrid.Rows[x].Cells[0].Value.ToString() + "__2", dgrid.Rows[x].Cells[1].Value == null ? "" : dgrid.Rows[x].Cells[1].Value.ToString());
                                    }
                                }
                                else
                                {
                                    if (field1 != 0)
                                    {
                                        pdfFormFields.SetField("mc_" + dgrid.Rows[x].Cells[0].Value.ToString() + "__2", dgrid.Rows[x].Cells[1].Value == null ? "" : dgrid.Rows[x].Cells[1].Value.ToString());
                                    }
                                }
                            }
                        }
                        else if (dgrid.Rows[x].Cells[0].Value.ToString().IndexOf("_cb") > -1)
                        {
                            string query = "select * from tbl_VPR_Mapping_Conditions where field_name = '" + dgrid.Rows[x].Cells[0].Value.ToString().Replace("_cb", "") + "'";
                            DataTable dt1 = cls.GetData(query);

                            string optr, col_name, col_val, cond, optr2, col_name2, col_val2, cond2, optr3, col_name3, col_val3, cond3 = "";
                            int a = 0, b = 0, c = 0, ctf = 0;

                            if (dt1.Rows.Count > 0)
                            {
                                if (dt1.Rows[0]["column_name"].ToString() != "")
                                {
                                    if (dt1.Rows[0]["column_name"].ToString() == "Property Type")
                                    {
                                        string qryNorm = "select * from tbl_VPR_PropertyType_Normalize where property_type = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        if (dtNorm.Rows.Count > 0)
                                        {
                                        col_name = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                        else
                                        {
                                            col_name = "NULL";
                                        }
                                        

                                    }
                                    else if (dt1.Rows[0]["column_name"].ToString() == "Occupancy Status")
                                    {
                                        string qryNorm = "select * from tbl_VPR_OccupancyStatus_Normalize where occupancy_status = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        col_name = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                    else
                                    {
                                        col_name = dtInflow.Rows[0][dt1.Rows[0]["column_name"].ToString()].ToString();
                                    }
                                    optr = dt1.Rows[0]["logical_operator"].ToString();
                                    col_val = dt1.Rows[0]["column_val"].ToString();

                                    if (!CheckCondition(optr, col_name, col_val))
                                    {
                                        a++;
                                    }
                                }

                                if (dt1.Rows[0]["column_name2"].ToString() != "")
                                {
                                    if (dt1.Rows[0]["column_name2"].ToString() == "Property Type")
                                    {
                                        string qryNorm = "select * from tbl_VPR_PropertyType_Normalize where property_type = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name2"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        if (dtNorm.Rows.Count > 0)
                                        {
                                        col_name2 = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                        else
                                        {
                                            col_name2 = "NULL";
                                        }
                                        
                                    }
                                    else if (dt1.Rows[0]["column_name2"].ToString() == "Occupancy Status")
                                    {
                                        string qryNorm = "select * from tbl_VPR_OccupancyStatus_Normalize where occupancy_status = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name2"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        col_name2 = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                    else
                                    {
                                        col_name2 = dtInflow.Rows[0][dt1.Rows[0]["column_name2"].ToString()].ToString();
                                    }
                                    optr2 = dt1.Rows[0]["logical_operator2"].ToString();
                                    col_val2 = dt1.Rows[0]["column_val2"].ToString();

                                    if (!CheckCondition(optr2, col_name2, col_val2))
                                    {
                                        b++;
                                    }
                                }

                                if (dt1.Rows[0]["column_name3"].ToString() != "")
                                {
                                    if (dt1.Rows[0]["column_name3"].ToString() == "Property Type")
                                    {
                                        string qryNorm = "select * from tbl_VPR_PropertyType_Normalize where property_type = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name3"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        //col_name3 = dtNorm.Rows[0]["normalize"].ToString();
                                        if (dtNorm.Rows.Count > 0)
                                        {
                                        col_name3 = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                        else
                                        {
                                            col_name3 = "NULL";
                                        }
                                    }
                                    else if (dt1.Rows[0]["column_name3"].ToString() == "Occupancy Status")
                                    {
                                        string qryNorm = "select * from tbl_VPR_OccupancyStatus_Normalize where occupancy_status = '" + dtInflow.Rows[0][dt1.Rows[0]["column_name3"].ToString()].ToString() + "'";
                                        DataTable dtNorm = cls.GetData(qryNorm);
                                        col_name3 = dtNorm.Rows[0]["normalize"].ToString();
                                    }
                                    else
                                    {
                                        col_name3 = dtInflow.Rows[0][dt1.Rows[0]["column_name3"].ToString()].ToString();
                                    }
                                    optr3 = dt1.Rows[0]["logical_operator3"].ToString();
                                    col_val3 = dt1.Rows[0]["column_val3"].ToString();

                                    if (!CheckCondition(optr3, col_name3, col_val3))
                                    {
                                        c++;
                                    }
                                }

                                cond = dt1.Rows[0]["and_or_val"].ToString();
                                cond2 = dt1.Rows[0]["and_or_val2"].ToString();

                                int field1 = (a > 0) ? 0 : 1;
                                int field2 = (b > 0) ? 0 : 1;
                                int field3 = (c > 0) ? 0 : 1;

                                DataTable dtLogic = new DataTable();

                                if (cond != "")
                                {
                                    string qryLogic = "select * from tbl_VPR_Logic where ";
                                    qryLogic += "a = '" + field1 + "' and optr1 = '" + cond.Trim().ToLower() + "' and b = '" + field2 + "' and optr2 = '" + cond2.Trim().ToLower() + "' and c = '" + field3 + "'";
                                    dtLogic = cls.GetData(qryLogic);
                                }

                                if (dtLogic.Rows.Count > 0)
                                {
                                    if (dtLogic.Rows[0]["action"].ToString() == "1")
                                    {
                                        pdfFormFields.SetField("mc_" + dgrid.Rows[x].Cells[0].Value.ToString(), dgrid.Rows[x].Cells[1].Value == null ? "" : dgrid.Rows[x].Cells[1].Value.ToString());

                                        pdfFormFields.SetField("mc_" + dgrid.Rows[x].Cells[0].Value.ToString() + "__2", dgrid.Rows[x].Cells[1].Value == null ? "" : dgrid.Rows[x].Cells[1].Value.ToString());
                                    }
                                }
                                else
                                {
                                    if (field1 != 0)
                                    {
                                        pdfFormFields.SetField("mc_" + dgrid.Rows[x].Cells[0].Value.ToString(), dgrid.Rows[x].Cells[1].Value == null ? "" : dgrid.Rows[x].Cells[1].Value.ToString());

                                        pdfFormFields.SetField("mc_" + dgrid.Rows[x].Cells[0].Value.ToString() + "__2", dgrid.Rows[x].Cells[1].Value == null ? "" : dgrid.Rows[x].Cells[1].Value.ToString());
                                    }
                                }
                            }
                        }
                        else
                        {
                            int fk = 0;

                            for (int f = 0; f <= dgrid1.Rows.Count - 1; f++)
                            {

                                if (dgrid.Rows[x].Cells[0].Value.ToString() == dgrid1.Rows[f].Cells[0].Value.ToString().Replace("__2", ""))
                                {
                                    fk += 1;
                                }

                            }

                            if (fk == 0)
                            {
                                pdfFormFields.SetField(dgrid.Rows[x].Cells[0].Value.ToString(), dgrid.Rows[x].Cells[1].Value == null ? "" : dgrid.Rows[x].Cells[1].Value.ToString());
                            }
                            else
                            {
                                pdfFormFields.SetField(dgrid.Rows[x].Cells[0].Value.ToString() + "__2", dgrid.Rows[x].Cells[1].Value == null ? "" : dgrid.Rows[x].Cells[1].Value.ToString());
                            }
                        }
                    }
                }

                //if (dgrid1.Rows.Count > 0)
                //{
                //    for (int x = 0; x < dgrid1.Rows.Count; x++)
                //    {
                //        pdfFormFields.SetField(dgrid1.Rows[x].Cells[0].Value.ToString(), dgrid1.Rows[x].Cells[1].Value == null ? "" : dgrid1.Rows[x].Cells[1].Value.ToString());
                //    }
                //}

                pdfStamper.FormFlattening = false;

                pdfStamper.Close();
                pdfReader.Close();

                txtFile.Text = newFile;
                webRight.Navigate(newFile + "#toolbar=0&navpanes=0");
            }
            catch
            {
                clsConnection cls = new clsConnection();
                MessageBox.Show("No available file for  " + arrInflow[0] + " | " + arrInflow[1] + " | " + arrInflow[2] + " | " + arrInflow[3] + " | " + arrInflow[4]);

                string qry1 = "update tbl_VPR_CreatedFile set status='Processed',processedBy='No template',InitialProcessor='" + Environment.UserName + "',processedDate='" + DateTime.Now + "',HandleTime='" + lblHT.Text + "'  where id='" + lblId.Text + "'";
                cls.ExecuteQuery(qry1);

                cls.ExecuteQuery("update tbl_VPR_CreatedFile set lockto='' where lockto='" + Environment.UserName + "'");

                this.Close();

                //Process.Start("ProChampsTest.exe");
                Process.Start("ProChamps.exe");
            }
        }

        public void getFromDB()
        {
            clsConnection cls = new clsConnection();
            cls.setConn("DAV");
            DataTable dt = new DataTable();
            string qry1 = "";

            var timeUTC = DateTime.UtcNow;
            TimeZoneInfo ezone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime etime = TimeZoneInfo.ConvertTimeFromUtc(timeUTC, ezone);

            for (int x = 0; x < dgrid.Rows.Count; x++)
            {
                qry1 = "select * from tbl_VPR_ClientLookup where clientcode ='" + dgrid.Rows[x].Cells[0].Value + "'";
                dt = cls.GetData(qry1);
                if (dt.Rows.Count > 0)
                {
                    dgrid.Rows[x].Cells[1].Value = dt.Rows[0][1].ToString();
                }

                string qry2 = "select * from tbl_VPR_Mapping_Conditions where ";
                qry2 += "field_name = REPLACE('" + dgrid.Rows[x].Cells[0].Value + "', '_txt', '') OR ";
                qry2 += "field_name = REPLACE('" + dgrid.Rows[x].Cells[0].Value + "', '_yn', '') OR ";
                qry2 += "field_name = REPLACE('" + dgrid.Rows[x].Cells[0].Value + "', '_cb', '')";

                DataTable dt2 = cls.GetData(qry2);

                if (dgrid.Rows[x].Cells[0].Value.ToString().IndexOf("_txt") > -1)
                {
                    dgrid.Rows[x].Cells[1].Value = dt2.Rows[0]["text_val"].ToString();
                }
                else if (dgrid.Rows[x].Cells[0].Value.ToString().IndexOf("_yn") > -1)
                {
                    dgrid.Rows[x].Cells[1].Value = dt2.Rows[0]["yes_no_val"].ToString();
                }
                else if (dgrid.Rows[x].Cells[0].Value.ToString().IndexOf("_cb") > -1)
                {
                    dgrid.Rows[x].Cells[1].Value = dt2.Rows[0]["chkbox_val"].ToString();
                }
            }

            qry1 = "select top 1 * from view_VPR_Inflow where lockto ='" + Environment.UserName + "' order by case when [Prio Tag] is null then 1 else 0 end, [Prio Tag], [Total Aging] desc"; //lblId.Text + "'";
            //qry1 = "select top 1 * from view_VPR_Inflow where [Property ID] = '32963183'"; //lblId.Text + "'";
            dtInflow = cls.GetData(qry1);

            for (int x = 0; x < dgrid.Rows.Count; x++)
            {

                try
                {
                    if (dgrid.Rows[x].Cells[0].Value.ToString() == "Asset Manager")
                    {
                        if (dtInflow.Rows[0]["Product"].ToString() == "REO" &&
                            (dtInflow.Rows[0]["Asset Manager"].ToString() == "PPI Asset" ||
                            dtInflow.Rows[0]["Asset Manager"].Equals(DBNull.Value)))
                        {
                            dgrid.Rows[x].Cells[1].Value = "Samir Shaikh";
                        }
                        else
                        {
                            dgrid.Rows[x].Cells[1].Value = dtInflow.Rows[0]["Asset Manager"].ToString();
                        }
                    }
                    else if (dgrid.Rows[x].Cells[0].Value.ToString() == "Current Date")
                    {
                        dgrid.Rows[x].Cells[1].Value = etime.ToString("MM/dd/yyyy");
                    }
                    else
                    {
                        string col_name = dgrid.Rows[x].Cells[0].Value.ToString();
                        if (col_name.IndexOf("mc_") > -1 || col_name.IndexOf("_txt") > -1 ||
                            col_name.IndexOf("_yn") > -1 || col_name.IndexOf("_cb") > -1)
                        {
                            col_name = col_name.Remove(col_name.IndexOf("_"), col_name.Length - col_name.IndexOf("_"));
                            string qry = "select * from tbl_VPR_Mapping_Conditions where field_name = '" + col_name + "'";

                            DataTable dtGet = cls.GetData(qry);

                            if (col_name.IndexOf("_txt") > -1)
                            {
                                dgrid.Rows[x].Cells[1].Value = dtGet.Rows[0]["text_val"].ToString();
                            }
                            else if (col_name.IndexOf("_yn") > -1)
                            {
                                dgrid.Rows[x].Cells[1].Value = dtGet.Rows[0]["yes_no_val"].ToString();
                            }
                            else if (col_name.IndexOf("_cb") > -1)
                            {
                                dgrid.Rows[x].Cells[1].Value = dtGet.Rows[0]["chkbox_val"].ToString();
                            }
                        }
                        else
                        {
                            dgrid.Rows[x].Cells[1].Value = dtInflow.Rows[0][col_name].ToString();
                        }
                    }
                }

                catch
                {

                }
            }

            //return dt;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            webRight.Hide();
            webRight.Navigate("about:blank");
            do
            {
                Application.DoEvents();
                System.Threading.Thread.Sleep(100);

            } while (webRight.ReadyState != WebBrowserReadyState.Complete);

            System.Threading.Thread.Sleep(100);
            webRight.Navigate("about:blank");
            webRight.Show();


            string pdfTemplate = lblPath.Text;//cboPath.Items[cboTemplate.SelectedIndex].ToString();
            string newFile = System.IO.Directory.GetCurrentDirectory() + "_New2.pdf";
            PdfReader pdfReader = new PdfReader(pdfTemplate);
            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(
                newFile, FileMode.Create));
            AcroFields pdfFormFields = pdfStamper.AcroFields;


            for (int x = 0; x < dgrid1.Rows.Count; x++)
            {
                String a = dgrid1.Rows[x].Cells[0].Value.ToString();

                a = a.Substring(0, a.IndexOf("__"));

                if (a != "")
                {
                    for (int y = 0; y < dgrid.Rows.Count; y++)
                    {
                        if (dgrid.Rows[y].Cells[0].Value.ToString() == a)
                        {
                            dgrid1.Rows[x].Cells[1].Value = dgrid.Rows[y].Cells[1].Value;
                        }
                    }
                }

            }

            for (int x = 0; x < dgrid.Rows.Count; x++)
            {
                pdfFormFields.SetField(dgrid.Rows[x].Cells[0].Value.ToString() + "__2", dgrid.Rows[x].Cells[1].Value == null ? "" : dgrid.Rows[x].Cells[1].Value.ToString());
            }


            for (int x = 0; x < dgrid1.Rows.Count; x++)
            {
                pdfFormFields.SetField(dgrid1.Rows[x].Cells[0].Value.ToString() + "__2", dgrid1.Rows[x].Cells[1].Value == null ? "" : dgrid1.Rows[x].Cells[1].Value.ToString());
            }


            pdfStamper.FormFlattening = false;
            pdfStamper.Close();
            txtFile.Text = newFile;
            webRight.Navigate(newFile + "#toolbar=0&navpanes=0");
        }
        public void savingdata (){

clsConnection cls = new clsConnection();
            cls.setConn("DAV");

            //// Displays a SaveFileDialog so the user can save the Image  

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "PDF file|*.pdf";
            saveFileDialog1.Title = "Save an PDF File";

            string qry = "select * from tbL_VPR_CreatedFile where id='" + lblId.Text + "'";
            DataTable dt = cls.GetData(qry);

            string propID = dt.Rows[0]["Property ID"].ToString();

            string qryMuni = "select municipality_name from tbl_VPR_Municipality where municipality_code = '" + dt.Rows[0]["Municipality Code"].ToString() + "'";
            DataTable dtMuni = cls.GetData(qryMuni);

            string fn = "";

            if (ctTag)
            {
                fn = "CT - Statewide_" + propID + "_VPR_PDF_RegForm_" + DateTime.Now.ToString("MMddyy");
            }
            else
            {
                fn = dtMuni.Rows[0]["municipality_name"].ToString() + "_" + propID + "_VPR_PDF_RegForm_" + DateTime.Now.ToString("MMddyy");
            }

            saveFileDialog1.FileName = fn;
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.  
            if (saveFileDialog1.FileName != "")
            {
                File.Copy(txtFile.Text, saveFileDialog1.FileName, true);

                string fillTag = "";
                string completeTag = "";

                if (rbGood.Checked)
                {
                    fillTag = rbGood.Text;
                }
                else
                {
                    //fillTag = rbError.Text + " - " + cbErrors.SelectedItem.ToString();
                    fillTag = rbError.Text;
                }

                if (rbComplete.Checked)
                {
                    completeTag = "Yes";
                }
                else
                {
                    completeTag = cbUncomplete.SelectedText;
                }


                string qry1 = "";

                if (ctTag)
                {
                    qry1 = "update tbl_VPR_CreatedFile set fillTag = '" + fillTag + "', [Complete Form] = '" + completeTag + "',processedBy='" + Environment.UserName + "',InitialProcessor='" + Environment.UserName + "',processedDate='" + DateTime.Now + "',HandleTime='" + lblHT.Text + "'  where id='" + lblId.Text + "'";

                    cls.ExecuteQuery(qry1);

                    ctDone = true;
                    //btnLoad_Click(sender, e);

                }
                else
                {
                    qry1 = "update tbl_VPR_CreatedFile set status='Processed', fillTag = '" + fillTag + "', [Complete Form] = '" + completeTag + "',processedBy='" + Environment.UserName + "',InitialProcessor='" + Environment.UserName + "',processedDate='" + DateTime.Now + "',HandleTime='" + lblHT.Text + "'  where id='" + lblId.Text + "'";

                    cls.ExecuteQuery(qry1);

                    cls.ExecuteQuery("update tbl_VPR_CreatedFile set lockto='' where lockto='" + Environment.UserName + "' and id = " + lblId.Text);

                    this.Close();

                    //Process.Start("FormFillerTest.exe");
                    Process.Start("ProChamps.exe");
                }

            }


       }
        public void button1_Click_1(object sender, EventArgs e)
        {
            clsConnection cls = new clsConnection();
            cls.setConn("DAV");


            DataTable TESTID = new DataTable();


            string query1 = "select top 1 * from tbl_VPR_CreatedFile where Status='Completed' and [Registration Process] = 'pdf'";
            TESTID = cls.GetData(query1);


            lblId.Text = TESTID.Rows[0]["id"].ToString();


            //// Displays a SaveFileDialog so the user can save the Image  

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "PDF file|*.pdf";
            saveFileDialog1.Title = "Save an PDF File";

            string qry = "select * from tbL_VPR_CreatedFile where id='" + lblId.Text + "'";
            DataTable dt = cls.GetData(qry);

            string propID = dt.Rows[0]["Property ID"].ToString();

            string qryMuni = "select municipality_name from tbl_VPR_Municipality where municipality_code = '" + dt.Rows[0]["Municipality Code"].ToString() + "'";
            DataTable dtMuni = cls.GetData(qryMuni);

            string fn = "";

            if (ctTag)
            {
                fn = "CT - Statewide_" + propID + "_VPR_PDF_RegForm_" + DateTime.Now.ToString("MMddyy");
            }
            else
            {
                fn = dtMuni.Rows[0]["municipality_name"].ToString() + "_" + propID + "_VPR_PDF_RegForm_" + DateTime.Now.ToString("MMddyy");
            }

            saveFileDialog1.FileName = fn;
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.  
            if (saveFileDialog1.FileName != "")
            {
                File.Copy(txtFile.Text, saveFileDialog1.FileName, true);

                string fillTag = "";
                string completeTag = "";

                if (rbGood.Checked)
                {
                    fillTag = rbGood.Text;
                }
                else
                {
                    //fillTag = rbError.Text + " - " + cbErrors.SelectedItem.ToString();
                    fillTag = rbError.Text;
                }

                if (rbComplete.Checked)
                {
                    completeTag = "Yes";
                }
                else
                {
                    completeTag = cbUncomplete.SelectedText;
                }


                string qry1 = "";

                if (ctTag)
                {
                    qry1 = "update tbl_VPR_CreatedFile set fillTag = '" + fillTag + "', [Complete Form] = '" + completeTag + "',processedBy='" + Environment.UserName + "',InitialProcessor='" + Environment.UserName + "',processedDate='" + DateTime.Now + "',HandleTime='" + lblHT.Text + "'  where id='" + lblId.Text + "'";

                    cls.ExecuteQuery(qry1);

                    ctDone = true;
                    btnLoad_Click(sender, e);

                }
                else
                {
                    qry1 = "update tbl_VPR_CreatedFile set status='Processed', fillTag = '" + fillTag + "', [Complete Form] = '" + completeTag + "',processedBy='" + Environment.UserName + "',InitialProcessor='" + Environment.UserName + "',processedDate='" + DateTime.Now + "',HandleTime='" + lblHT.Text + "'  where id='" + lblId.Text + "'";

                    cls.ExecuteQuery(qry1);

                    cls.ExecuteQuery("update tbl_VPR_CreatedFile set lockto='' where lockto='" + Environment.UserName + "' and id = " + lblId.Text);

                    this.Close();

                    //Process.Start("FormFillerTest.exe");
                    Process.Start("ProChamps.exe");
                }

            }
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            var x = Int32.Parse(lblHT.Text);
            var y = x + 1;
            lblHT.Text = y.ToString();
        }

        private void webRight_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void dgrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgrid_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dgrid.Rows[e.RowIndex].Cells[1].Value.ToString().Contains("www."))
            {
                System.Diagnostics.Process.Start("http://google.com");
            }
        }
        
        private void rbGood_CheckedChanged(object sender, EventArgs e)
        {
            
            if (rbGood.Checked)
            {
                //cbErrors.Hide();
                //btnCorrect.Hide();
                asd.Hide();
            }
            else
            {
                //cbErrors.Show();
                //btnCorrect.Show();
                //asd.Show();
            }
        }

        private void rbError_CheckedChanged(object sender, EventArgs e)
        {
           
            if (rbError.Checked)
            {
                //cbErrors.Show();
                //btnCorrect.Show();
                asd.Show();
                
            }
            else
            {
                //cbErrors.Hide();
                asd.Hide();
                //btnCorrect.Hide();
            }
        }

        private void btnPrevPDF_Click(object sender, EventArgs e)
        {
            string fn = lblPath.Text;

            string newFile = System.IO.Directory.GetCurrentDirectory() + "_New4.pdf";

            populateDataGrid(fn, newFile, arrInflow);

            //txtFile.Text = newFile;
            //webRight.Navigate(newFile + "#toolbar=0&navpanes=0");
            button2.Enabled = true;
            button1.Enabled = true;
            btnLoad.Enabled = false;
        }

        private void btnNextPDF_Click(object sender, EventArgs e)
        {
            string file1 = @"\\ascorp.com\data\bangalore\CommonShare\Strategic$\Internal\Operations\VPR Manila\PPI-MIS Manila\Dump - Raw File\PDF Files\";
            string fn = "RI_000000053_CT - Statewide.pdf";

            string pdfTemplate = file1 + fn;

            string newFile = System.IO.Directory.GetCurrentDirectory() + "_New2.pdf";

            populateDataGrid(pdfTemplate, newFile, arrInflow);

            //txtFile.Text = newFile;
            //webRight.Navigate(newFile + "#toolbar=0&navpanes=0");
            button2.Enabled = true;
            button1.Enabled = true;
            btnLoad.Enabled = false;
        }

        public void AddToList(params string[] list)
        {
            foreach (string s in list)
            {
                arrInflow.Add(s);
            }
        }

        private void rbComplete_CheckedChanged(object sender, EventArgs e)
        {
            if (rbComplete.Checked)
            {
                cbUncomplete.Visible = false;
            }
            else
            {
                cbUncomplete.Visible = true;
            }
        }

        private void rbUncomplete_CheckedChanged(object sender, EventArgs e)
        {
            if (rbUncomplete.Checked)
            {
                cbUncomplete.Visible = true;
            }
            else
            {
                cbUncomplete.Visible = false;
            }
        }

        public bool CheckCondition(string optr, string colname, string colval)
        {
            bool cond = false;

            if (optr == "equal")
            {
                if (colname == colval)
                {
                    cond = true;
                }
            }
            else if (optr == "not equal")
            {
                if (colname != colval)
                {
                    cond = true;
                }
            }
            else if (optr == "greater than")
            {
                if (Convert.ToInt32(colname) > Convert.ToInt32(colval))
                {
                    cond = true;
                }
            }
            else if (optr == "less than")
            {
                if (Convert.ToInt32(colname) < Convert.ToInt32(colval))
                {
                    cond = true;
                }
            }
            else if (optr == "contains")
            {
                if (colname.IndexOf(colval) > -1)
                {
                    cond = true;
                }
            }

            return cond;
        }

        private void btnCorrect_Click(object sender, EventArgs e)
        {
            
            form2.Show();
        }

        private void rbError_Click(object sender, EventArgs e)
        {
            
            if (rbError.Checked)
            {
                asd.Show();
            }
            else
            {
                asd.Hide();
            }
        }

        public void button3_Click(object sender, EventArgs e)
        {

            button1.PerformClick();

            //var frm = (Form1)this.Owner;
            //if (frm != null)
            //    frm.button3.PerformClick();
            MessageBox.Show("ok");
        }
    }
}
