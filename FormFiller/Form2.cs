﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using iTextSharp.text.pdf;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Collections;

namespace FormFiller
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }


        Label label;
        CheckBox box;
        CheckBox box2;
        CheckBox box3;
        private void Form2_Load(object sender, EventArgs e)
        {
            this.Show();
            clsConnection cls = new clsConnection();
            cls.setConn("DAV");

            //CheckBox box3;
            for (int i = 0; i < Form1.arrList1.Count; i++)
            {

                label = new Label();
                box = new CheckBox();
                box2 = new CheckBox();
                box3 = new CheckBox();
                label.Text = Form1.arrList1[i].ToString();
                box.Tag = Form1.arrList1[i].ToString();
                box2.Tag = Form1.arrList1[i].ToString();
                box3.Tag = Form1.arrList1[i].ToString();
                label.AutoSize = false;
                box.AutoSize = true;
                box2.AutoSize = true;
                box3.AutoSize = true;
                label.Size = new System.Drawing.Size(250, 20);
                flowLayoutPanel4.Controls.Add(label);
                flowLayoutPanel1.Controls.Add(box);
                flowLayoutPanel2.Controls.Add(box2);
                flowLayoutPanel3.Controls.Add(box3);
            }
            flowLayoutPanel4.AutoScroll = true;
            flowLayoutPanel1.AutoScroll = true;
            flowLayoutPanel2.AutoScroll = true;
            flowLayoutPanel3.AutoScroll = true;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            clsConnection cls = new clsConnection();
            cls.setConn("DAV");
            string font = "", query = "", content = "", checkquery = "", blank = "";
            ArrayList fontError = new ArrayList();

            foreach (Control c in flowLayoutPanel1.Controls)
            {
                if ((c is CheckBox) && ((CheckBox)c).Checked)
                {
                    font += c.Tag + ",";
                }
            }
            foreach (Control c in flowLayoutPanel2.Controls)
            {
                if ((c is CheckBox) && ((CheckBox)c).Checked)
                {
                    content += c.Tag + ",";
                }
            }
            foreach (Control c in flowLayoutPanel3.Controls)
            {
                if ((c is CheckBox) && ((CheckBox)c).Checked)
                {
                    blank += c.Tag + ",";
                }
            }

            if (font != "" || content != "" || blank != "")
            {
                if (font.EndsWith(","))
                {
                    font = font.Remove(font.Length - 1, 1);
                }
                if (content.EndsWith(","))
                {
                    content = content.Remove(content.Length - 1, 1);
                }
                if (blank.EndsWith(","))
                {
                    blank = blank.Remove(blank.Length - 1, 1);
                }

                checkquery = "select * from tbl_VPR_FormFiller_Error where id = '" + Form1.id + "'";
                DataTable dt = cls.GetData(checkquery);
                if (checkBox1.Checked == true)
                {
                    if (dt.Rows.Count > 0)
                    {
                        query = "update tbl_VPR_FormFiller_Error set [Font Error] = '" + font + "', [Content Error] = '" + content
                                + "', [Blank Error] = '" + blank + "',modified_by = '" + Environment.UserName + "', date_modified = CAST(GETDATE() as datetime) where id = '" + Form1.id + "';";
                    }
                    else
                    {
                        query = "insert tbl_VPR_FormFiller_Error (id,[Property ID],[Font Error],[Content Error],[Blank Error],date_modified,modified_by) values ('"
                            + Form1.id + "','" + Form1.propID + "','" + font + "','" + content + "','" + blank + "', CAST(GETDATE() as datetime),'" + Environment.UserName + "')";
                    }

                    int i = cls.ExecuteQuery(query);

                    MessageBox.Show("Correction saved.");
                    this.Hide();

                }
                else
                {
                    if (dt.Rows.Count > 0)
                    {
                        query = "update tbl_VPR_FormFiller_Error set [Font Error] = '" + font + "', [Content Error] = '" + content + "', [Blank Error] = '" + blank + "',modified_by = '" + Environment.UserName + "',[No_Mapping]='" + textBox1.Text + "',[Wrong_Form]='', date_modified = CAST(GETDATE() as datetime) where id = '" + Form1.id + "';";
                    }
                    else
                    {
                        query = "insert tbl_VPR_FormFiller_Error (id,[Property ID],[Font Error],[Content Error],[Blank Error],date_modified,modified_by,[Wrong_Form],[No_Mapping]) values ('"
                            + Form1.id + "','" + Form1.propID + "','" + font + "','" + content + "','" + blank + "', CAST(GETDATE() as datetime),'" + Environment.UserName + "','', '" + textBox1.Text + "')";
                    }

                    int i = cls.ExecuteQuery(query);

                    MessageBox.Show("Correction saved.");
                    this.Hide();

                }
            }
            else
            {
                MessageBox.Show("No error was selected", "Error");

            }



        }

        private void flowLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void flowLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                flowLayoutPanel1.Enabled = false;
                flowLayoutPanel2.Enabled = false;
                flowLayoutPanel3.Enabled = false;
                flowLayoutPanel4.Enabled = false;
                textBox1.Enabled = false;
                btnwrongformloaded.Show();
                button1.Hide();

                //foreach (Control c in flowLayoutPanel1.Controls)
                //{
                //    if ((c is CheckBox) && ((CheckBox)c).Checked)
                //    {


                //    }
                //} 
            }
            else
            {
                flowLayoutPanel1.Enabled = true;
                flowLayoutPanel2.Enabled = true;
                flowLayoutPanel3.Enabled = true;
                flowLayoutPanel4.Enabled = true;
                textBox1.Enabled = true;
                btnwrongformloaded.Hide();
                button1.Show();


            }
        }
    


        public void btnwrongformloaded_Click(object sender, EventArgs e)
        {





            Form1 frm = new Form1();

  
            //frm.Show(this);
            
         
                frm.button1_Click_1(sender, e);


            clsConnection cls = new clsConnection();

            cls.setConn("DAV");
            string font = "", query = "", content = "", checkquery = "", blank = "";
            ArrayList fontError = new ArrayList();





            if (font.EndsWith(","))
            {
                font = font.Remove(font.Length - 1, 1);
            }
            if (content.EndsWith(","))
            {
                content = content.Remove(content.Length - 1, 1);
            }
            if (blank.EndsWith(","))
            {
                blank = blank.Remove(blank.Length - 1, 1);
            }

            checkquery = "select * from tbl_VPR_FormFiller_Error where id = '" + Form1.id + "'";
            DataTable dt = cls.GetData(checkquery);
            if (checkBox1.Checked == true)
            {


                if (dt.Rows.Count > 0)
                {
                    query = "update tbl_VPR_FormFiller_Error set [Font Error] = '', [Content Error] = '', [Blank Error] = '',modified_by = '" + Environment.UserName + "',File_name='" + Form1.nameofpdf + "', date_modified = CAST(GETDATE() as datetime),Wrong_Form = '1',[No_Mapping]= '' where id = '" + Form1.id + "';";
                }
                else
                {
                    query = "insert tbl_VPR_FormFiller_Error (id,[Property ID],[Font Error],[Content Error],[Blank Error],date_modified,modified_by,[Wrong_Form],[No_Mapping],File_name) values ('" + Form1.id + "','" + Form1.propID + "','','','', CAST(GETDATE() as datetime),'" + Environment.UserName + "','1','','" + Form1.nameofpdf + "')";
                }

                int i = cls.ExecuteQuery(query);

                MessageBox.Show("Correction saved.");
                this.Hide();

            }
            else
            {


                if (dt.Rows.Count > 0)
                {
                    query = "update tbl_VPR_FormFiller_Error set [Font Error] = '" + font + "', [Content Error] = '" + content
                            + "', [Blank Error] = '" + blank + "',modified_by = '" + Environment.UserName + "', date_modified = CAST(GETDATE() as datetime) where id = '" + Form1.id + "';";
                }
                else
                {
                    query = "insert tbl_VPR_FormFiller_Error (id,[Property ID],[Font Error],[Content Error],[Blank Error],date_modified,modified_by) values ('"
                        + Form1.id + "','" + Form1.propID + "','" + font + "','" + content + "','" + blank + "', CAST(GETDATE() as datetime),'" + Environment.UserName + "')";
                }

                int i = cls.ExecuteQuery(query);

                MessageBox.Show("Correction saved.");
                this.Hide();

            }


            

          




        }


        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }
        }



    }

}

