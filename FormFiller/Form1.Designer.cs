﻿namespace FormFiller
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.lblProduct = new System.Windows.Forms.Label();
            this.lblMunicipality = new System.Windows.Forms.Label();
            this.lblProp = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbUncomplete = new System.Windows.Forms.RadioButton();
            this.rbComplete = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.cbUncomplete = new System.Windows.Forms.ComboBox();
            this.cbErrors = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbGood = new System.Windows.Forms.RadioButton();
            this.rbError = new System.Windows.Forms.RadioButton();
            this.btnNextPDF = new System.Windows.Forms.Button();
            this.btnPrevPDF = new System.Windows.Forms.Button();
            this.lblState = new System.Windows.Forms.Label();
            this.lblAdd = new System.Windows.Forms.Label();
            this.lblHT = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnLoad = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblPath = new System.Windows.Forms.Label();
            this.lblId = new System.Windows.Forms.Label();
            this.cboTemplate = new System.Windows.Forms.ComboBox();
            this.lblUser = new System.Windows.Forms.Label();
            this.cboPath = new System.Windows.Forms.ComboBox();
            this.dgrid = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgrid1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtRowCnt = new System.Windows.Forms.TextBox();
            this.txtRow = new System.Windows.Forms.TextBox();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.webRight = new System.Windows.Forms.WebBrowser();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel1_Paint);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgrid1);
            this.splitContainer1.Panel2.Controls.Add(this.txtRowCnt);
            this.splitContainer1.Panel2.Controls.Add(this.txtRow);
            this.splitContainer1.Panel2.Controls.Add(this.txtFile);
            this.splitContainer1.Panel2.Controls.Add(this.webRight);
            this.splitContainer1.Size = new System.Drawing.Size(1528, 731);
            this.splitContainer1.SplitterDistance = 656;
            this.splitContainer1.TabIndex = 7;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.lblId);
            this.splitContainer2.Panel1.Controls.Add(this.lblProduct);
            this.splitContainer2.Panel1.Controls.Add(this.lblMunicipality);
            this.splitContainer2.Panel1.Controls.Add(this.lblProp);
            this.splitContainer2.Panel1.Controls.Add(this.button1);
            this.splitContainer2.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer2.Panel1.Controls.Add(this.cbErrors);
            this.splitContainer2.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer2.Panel1.Controls.Add(this.btnNextPDF);
            this.splitContainer2.Panel1.Controls.Add(this.btnPrevPDF);
            this.splitContainer2.Panel1.Controls.Add(this.lblState);
            this.splitContainer2.Panel1.Controls.Add(this.lblAdd);
            this.splitContainer2.Panel1.Controls.Add(this.lblHT);
            this.splitContainer2.Panel1.Controls.Add(this.label2);
            this.splitContainer2.Panel1.Controls.Add(this.btnLoad);
            this.splitContainer2.Panel1.Controls.Add(this.button2);
            this.splitContainer2.Panel1.Controls.Add(this.panel1);
            this.splitContainer2.Panel1.Controls.Add(this.lblUser);
            this.splitContainer2.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer2_Panel1_Paint);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.cboPath);
            this.splitContainer2.Panel2.Controls.Add(this.dgrid);
            this.splitContainer2.Panel2.Controls.Add(this.button3);
            this.splitContainer2.Size = new System.Drawing.Size(656, 731);
            this.splitContainer2.SplitterDistance = 200;
            this.splitContainer2.TabIndex = 8;
            // 
            // lblProduct
            // 
            this.lblProduct.AutoSize = true;
            this.lblProduct.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProduct.Location = new System.Drawing.Point(227, 10);
            this.lblProduct.Name = "lblProduct";
            this.lblProduct.Size = new System.Drawing.Size(56, 15);
            this.lblProduct.TabIndex = 324;
            this.lblProduct.Text = "Product: ";
            // 
            // lblMunicipality
            // 
            this.lblMunicipality.AutoSize = true;
            this.lblMunicipality.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMunicipality.Location = new System.Drawing.Point(93, 64);
            this.lblMunicipality.Name = "lblMunicipality";
            this.lblMunicipality.Size = new System.Drawing.Size(81, 15);
            this.lblMunicipality.TabIndex = 323;
            this.lblMunicipality.Text = "Municipality:";
            // 
            // lblProp
            // 
            this.lblProp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblProp.Location = new System.Drawing.Point(96, 9);
            this.lblProp.Name = "lblProp";
            this.lblProp.ReadOnly = true;
            this.lblProp.Size = new System.Drawing.Size(100, 13);
            this.lblProp.TabIndex = 322;
            this.lblProp.TabStop = false;
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(16, 180);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 27);
            this.button1.TabIndex = 15;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbUncomplete);
            this.groupBox2.Controls.Add(this.rbComplete);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.cbUncomplete);
            this.groupBox2.Location = new System.Drawing.Point(16, 120);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(402, 58);
            this.groupBox2.TabIndex = 321;
            this.groupBox2.TabStop = false;
            // 
            // rbUncomplete
            // 
            this.rbUncomplete.AutoSize = true;
            this.rbUncomplete.Location = new System.Drawing.Point(360, 10);
            this.rbUncomplete.Name = "rbUncomplete";
            this.rbUncomplete.Size = new System.Drawing.Size(39, 17);
            this.rbUncomplete.TabIndex = 320;
            this.rbUncomplete.Text = "No";
            this.rbUncomplete.UseVisualStyleBackColor = true;
            this.rbUncomplete.CheckedChanged += new System.EventHandler(this.rbUncomplete_CheckedChanged);
            // 
            // rbComplete
            // 
            this.rbComplete.AutoSize = true;
            this.rbComplete.Checked = true;
            this.rbComplete.Location = new System.Drawing.Point(311, 10);
            this.rbComplete.Name = "rbComplete";
            this.rbComplete.Size = new System.Drawing.Size(43, 17);
            this.rbComplete.TabIndex = 23;
            this.rbComplete.TabStop = true;
            this.rbComplete.Text = "Yes";
            this.rbComplete.UseVisualStyleBackColor = true;
            this.rbComplete.CheckedChanged += new System.EventHandler(this.rbComplete_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(299, 13);
            this.label1.TabIndex = 319;
            this.label1.Text = "Do you have all the information required to complete the form?";
            // 
            // cbUncomplete
            // 
            this.cbUncomplete.FormattingEnabled = true;
            this.cbUncomplete.Items.AddRange(new object[] {
            "Need information from primary vendor",
            "Need book and page number",
            "Need FITNO",
            "Need LP document",
            "Need recordation date",
            "Need copy of deed - investor code C14911"});
            this.cbUncomplete.Location = new System.Drawing.Point(9, 26);
            this.cbUncomplete.Name = "cbUncomplete";
            this.cbUncomplete.Size = new System.Drawing.Size(223, 21);
            this.cbUncomplete.TabIndex = 318;
            this.cbUncomplete.Visible = false;
            // 
            // cbErrors
            // 
            this.cbErrors.FormattingEnabled = true;
            this.cbErrors.Items.AddRange(new object[] {
            "Font Error",
            "Content Error",
            "Content and Font Error"});
            this.cbErrors.Location = new System.Drawing.Point(424, 157);
            this.cbErrors.Name = "cbErrors";
            this.cbErrors.Size = new System.Drawing.Size(57, 21);
            this.cbErrors.TabIndex = 22;
            this.cbErrors.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbGood);
            this.groupBox1.Controls.Add(this.rbError);
            this.groupBox1.Location = new System.Drawing.Point(16, 81);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(308, 33);
            this.groupBox1.TabIndex = 320;
            this.groupBox1.TabStop = false;
            // 
            // rbGood
            // 
            this.rbGood.AutoSize = true;
            this.rbGood.Checked = true;
            this.rbGood.Location = new System.Drawing.Point(6, 10);
            this.rbGood.Name = "rbGood";
            this.rbGood.Size = new System.Drawing.Size(77, 17);
            this.rbGood.TabIndex = 20;
            this.rbGood.TabStop = true;
            this.rbGood.Text = "Good Form";
            this.rbGood.UseVisualStyleBackColor = true;
            this.rbGood.CheckedChanged += new System.EventHandler(this.rbGood_CheckedChanged);
            // 
            // rbError
            // 
            this.rbError.AutoSize = true;
            this.rbError.Location = new System.Drawing.Point(89, 10);
            this.rbError.Name = "rbError";
            this.rbError.Size = new System.Drawing.Size(99, 17);
            this.rbError.TabIndex = 21;
            this.rbError.TabStop = true;
            this.rbError.Text = "Form with errors";
            this.rbError.UseVisualStyleBackColor = true;
            this.rbError.CheckedChanged += new System.EventHandler(this.rbError_CheckedChanged);
            this.rbError.Click += new System.EventHandler(this.rbError_Click);
            // 
            // btnNextPDF
            // 
            this.btnNextPDF.Location = new System.Drawing.Point(601, 91);
            this.btnNextPDF.Name = "btnNextPDF";
            this.btnNextPDF.Size = new System.Drawing.Size(75, 23);
            this.btnNextPDF.TabIndex = 8;
            this.btnNextPDF.Text = "Next >>";
            this.btnNextPDF.UseVisualStyleBackColor = true;
            this.btnNextPDF.Visible = false;
            this.btnNextPDF.Click += new System.EventHandler(this.btnNextPDF_Click);
            // 
            // btnPrevPDF
            // 
            this.btnPrevPDF.Location = new System.Drawing.Point(525, 91);
            this.btnPrevPDF.Name = "btnPrevPDF";
            this.btnPrevPDF.Size = new System.Drawing.Size(75, 23);
            this.btnPrevPDF.TabIndex = 317;
            this.btnPrevPDF.Text = "<< Previous";
            this.btnPrevPDF.UseVisualStyleBackColor = true;
            this.btnPrevPDF.Visible = false;
            this.btnPrevPDF.Click += new System.EventHandler(this.btnPrevPDF_Click);
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblState.Location = new System.Drawing.Point(93, 27);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(37, 15);
            this.lblState.TabIndex = 19;
            this.lblState.Text = "State:";
            // 
            // lblAdd
            // 
            this.lblAdd.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdd.Location = new System.Drawing.Point(93, 44);
            this.lblAdd.Name = "lblAdd";
            this.lblAdd.Size = new System.Drawing.Size(322, 20);
            this.lblAdd.TabIndex = 17;
            this.lblAdd.Text = "Address:";
            // 
            // lblHT
            // 
            this.lblHT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHT.Font = new System.Drawing.Font("Trebuchet MS", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHT.Location = new System.Drawing.Point(427, 29);
            this.lblHT.Name = "lblHT";
            this.lblHT.Size = new System.Drawing.Size(90, 47);
            this.lblHT.TabIndex = 16;
            this.lblHT.Text = "-1";
            this.lblHT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(423, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Handle Time";
            // 
            // btnLoad
            // 
            this.btnLoad.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoad.Location = new System.Drawing.Point(12, 0);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 14;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Visible = false;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(12, 27);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 12;
            this.button2.Text = "Refill";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackgroundImage = global::FormFiller.Properties.Resources.Altisource_Logo;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.lblPath);
            this.panel1.Controls.Add(this.cboTemplate);
            this.panel1.Location = new System.Drawing.Point(522, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(130, 58);
            this.panel1.TabIndex = 8;
            // 
            // lblPath
            // 
            this.lblPath.AutoSize = true;
            this.lblPath.Location = new System.Drawing.Point(22, 43);
            this.lblPath.Name = "lblPath";
            this.lblPath.Size = new System.Drawing.Size(39, 13);
            this.lblPath.TabIndex = 1;
            this.lblPath.Text = "lblPath";
            this.lblPath.Visible = false;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(544, 126);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(35, 13);
            this.lblId.TabIndex = 0;
            this.lblId.Text = "label1";
            // 
            // cboTemplate
            // 
            this.cboTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.cboTemplate.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTemplate.FormattingEnabled = true;
            this.cboTemplate.Location = new System.Drawing.Point(33, 7);
            this.cboTemplate.Name = "cboTemplate";
            this.cboTemplate.Size = new System.Drawing.Size(94, 60);
            this.cboTemplate.TabIndex = 13;
            this.cboTemplate.Text = "Load Template";
            this.cboTemplate.Visible = false;
            // 
            // lblUser
            // 
            this.lblUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUser.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUser.Location = new System.Drawing.Point(522, 6);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(130, 23);
            this.lblUser.TabIndex = 9;
            this.lblUser.Text = "Aloba, Brian";
            this.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblUser.Click += new System.EventHandler(this.label2_Click);
            // 
            // cboPath
            // 
            this.cboPath.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.cboPath.FormattingEnabled = true;
            this.cboPath.Location = new System.Drawing.Point(230, 79);
            this.cboPath.Name = "cboPath";
            this.cboPath.Size = new System.Drawing.Size(213, 166);
            this.cboPath.TabIndex = 15;
            this.cboPath.Text = "Load Template";
            this.cboPath.Visible = false;
            // 
            // dgrid
            // 
            this.dgrid.AllowUserToAddRows = false;
            this.dgrid.AllowUserToDeleteRows = false;
            this.dgrid.AllowUserToResizeColumns = false;
            this.dgrid.AllowUserToResizeRows = false;
            this.dgrid.BackgroundColor = System.Drawing.Color.White;
            this.dgrid.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.dgrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgrid.GridColor = System.Drawing.Color.Silver;
            this.dgrid.Location = new System.Drawing.Point(0, 0);
            this.dgrid.Name = "dgrid";
            this.dgrid.RowHeadersVisible = false;
            this.dgrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Blue;
            this.dgrid.RowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dgrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrid.ShowEditingIcon = false;
            this.dgrid.Size = new System.Drawing.Size(656, 527);
            this.dgrid.TabIndex = 316;
            this.dgrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgrid_CellContentClick);
            this.dgrid.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgrid_CellMouseDoubleClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Form Field";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 250;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Field Value";
            this.Column2.Name = "Column2";
            this.Column2.Width = 300;
            // 
            // dgrid1
            // 
            this.dgrid1.AllowUserToAddRows = false;
            this.dgrid1.AllowUserToDeleteRows = false;
            this.dgrid1.AllowUserToResizeColumns = false;
            this.dgrid1.AllowUserToResizeRows = false;
            this.dgrid1.BackgroundColor = System.Drawing.Color.White;
            this.dgrid1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dgrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.dgrid1.GridColor = System.Drawing.Color.Silver;
            this.dgrid1.Location = new System.Drawing.Point(40, 204);
            this.dgrid1.Name = "dgrid1";
            this.dgrid1.RowHeadersVisible = false;
            this.dgrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Blue;
            this.dgrid1.RowsDefaultCellStyle = dataGridViewCellStyle14;
            this.dgrid1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrid1.ShowEditingIcon = false;
            this.dgrid1.Size = new System.Drawing.Size(409, 200);
            this.dgrid1.TabIndex = 318;
            this.dgrid1.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Form Field";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 250;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Field Value";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 300;
            // 
            // txtRowCnt
            // 
            this.txtRowCnt.Location = new System.Drawing.Point(78, 119);
            this.txtRowCnt.Name = "txtRowCnt";
            this.txtRowCnt.Size = new System.Drawing.Size(100, 20);
            this.txtRowCnt.TabIndex = 18;
            this.txtRowCnt.Visible = false;
            // 
            // txtRow
            // 
            this.txtRow.Location = new System.Drawing.Point(78, 81);
            this.txtRow.Name = "txtRow";
            this.txtRow.Size = new System.Drawing.Size(100, 20);
            this.txtRow.TabIndex = 17;
            this.txtRow.Text = "2";
            this.txtRow.Visible = false;
            // 
            // txtFile
            // 
            this.txtFile.Location = new System.Drawing.Point(78, 44);
            this.txtFile.Name = "txtFile";
            this.txtFile.Size = new System.Drawing.Size(100, 20);
            this.txtFile.TabIndex = 16;
            this.txtFile.Visible = false;
            // 
            // webRight
            // 
            this.webRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webRight.Location = new System.Drawing.Point(0, 0);
            this.webRight.MinimumSize = new System.Drawing.Size(20, 20);
            this.webRight.Name = "webRight";
            this.webRight.ScriptErrorsSuppressed = true;
            this.webRight.Size = new System.Drawing.Size(868, 731);
            this.webRight.TabIndex = 2;
            this.webRight.Url = new System.Uri("about:blank", System.UriKind.Absolute);
            this.webRight.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webRight_DocumentCompleted);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(427, 29);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 27);
            this.button3.TabIndex = 325;
            this.button3.Text = "Save";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1528, 731);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VPR Form Filler";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgrid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.WebBrowser webRight;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox cboTemplate;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.ComboBox cboPath;
        internal System.Windows.Forms.DataGridView dgrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.TextBox txtRow;
        private System.Windows.Forms.TextBox txtRowCnt;
        internal System.Windows.Forms.DataGridView dgrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label lblPath;
        private System.Windows.Forms.Label lblHT;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Label lblAdd;
        private System.Windows.Forms.ComboBox cbErrors;
        private System.Windows.Forms.Button btnPrevPDF;
        private System.Windows.Forms.Button btnNextPDF;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbUncomplete;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbUncomplete;
        private System.Windows.Forms.RadioButton rbComplete;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbGood;
        private System.Windows.Forms.RadioButton rbError;
        private System.Windows.Forms.TextBox lblProp;
        private System.Windows.Forms.Label lblMunicipality;
        private System.Windows.Forms.Label lblProduct;
        private System.Windows.Forms.Button button3;

    }
}

